package com.springboot.userapi.Users;

import java.util.Date;

public class User {
    private Integer id;
    private String name;
    private Date brithdate;

    public User(Integer id, String name, Date brithdate) {
        super();
        this.id = id;
        this.name = name;
        this.brithdate = brithdate;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBrithdate() {
        return brithdate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrithdate(Date brithdate) {
        this.brithdate = brithdate;
    }

    @Override
    public String toString() {
        return String.format("User [%d:%s]", id, name);
    }
}