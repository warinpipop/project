package com.springboot.userapi.Users;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class UserService {
    private int count = 3;
    private static List<User> users = new ArrayList<>();

    static {
        users.add(new User(1, "a", new Date()));
        users.add(new User(2, "s", new Date()));
        users.add(new User(3, "d", new Date()));
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) {
        if (user.getId() == null) {
            user.setId(++count);
        }
        System.out.println(user.getId());
        users.add(user);
        return user;
    }

    public User findOne(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}